# Scripts for deploying, modifying and deleting thingsboard cluster

## Cluster setup
If creating the cluster with a remote database (this would be used for production), skype the first step and do the following:    
- 	Install docker on a VM, make sure the kubernetes cluster has access to this VM     
- 	Pull the timescaledb database: `docker pull timescale/timescaledb` (use version number to specify exact release)    
- 	Start the database: `docker start timescaledb`    
- 	Log in to the db, create the dabase: `create database thingsboard_x_x_x`    
- 	Compile the source code of thingsboard: `mvn clean install -Dlicense.skip=true -DskipTests -Ddockerfile.skip=true`     
- 	Modify the url of databse line in `/application/src/main/resources/thingsboard.yml`, example: `url: "$    {SPRING_DATASOURCE_URL:jdbc:postgresql://localhost:5432/thingsboard_3_3_4_1_k3s}"`    
- 	Copy all the schemas to application package: `cp ./dao/src/main/resources/sql/* /application/src/main/data/sql/`    
- 	Run the `ThingsboardInstallApplication.java` class in the source code of thingsboard to install the required tables    
- 	If you installed the database in your dev environemnt timescaledb, export it and then import it in the VM database using the 
   documentation in:    
	`https://gitlab.com/iamus/iamus-thingsboard/-/wikis/PSQL-run,-backup-and-restore`    

Use the first script only if you want to run the database in the cluster!!!    
1) ./k8s-install-tb.sh    
Deploy kafka and redis before the thingsboard deployments, since they thingsboard depends on these services to start:    
2) * modify paramters (replica number etc.) in `thirdparty.yml` file     
   * run: ./k8s-deploy-thirdparty.sh    
Deploy the thingsboard pods:    
3) * modify jwt-token-signing-key-secret.yml to use for signing tokens    
   * modify database type and url in `postgres/tb-node-db-configmap.yml` file     
   * modify paramters (replica number etc.) in `tb-node.yml` file     
   * modify paramters (replica number etc.) in `thingsboard.yml` file     
   * run: ./k8s-deploy-resources.sh    
